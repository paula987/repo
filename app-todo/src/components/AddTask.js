import React, { Component } from 'react'
import './AddTask.css'
class AddTask extends Component {
    state = {
        text: '',
        checked: false,
        date: new Date().toISOString().slice(0, 10)
    }
    handleText = (e) => {
        this.setState({
            text: e.target.value
        })
    }
    handleCheckBox = (e) => {
        this.setState({
            checked: e.target.checked
        })
    }
    handleDate = (e) => {
        this.setState({
            date: e.target.value
        })
    }
    handleClick = () => {
        const { text, checked, date } = this.state
        if (text.length > 2 && text.length < 50) {
            this.props.add(text, date, checked)
            this.setState({
                text: '',
                checked: false,
                date: new Date().toISOString().slice(0, 10)
            })
        } else {
            alert("Za krótka lub długa wiadomość")
        }
    }
    render() {
        const minDate = new Date().toISOString().slice(0, 10)
        let maxDate = minDate.slice(0, 4) * 1 + 2
        maxDate = maxDate + "-12-31"

        return (
            <div>
                <input class="input-from" type="text" placeholder="Dodaj zadanie..." value={this.state.text} onChange={this.handleText} />
                <input type="checkbox" checked={this.state.checked} id="important" onChange={this.handleCheckBox} />
                <label htmlFor="important">Priorytet</label>
                <br />
                <label htmlFor="date">Do kiedy ma być wykonane zadanie: </label>
                <input class="input-from" type="date" value={this.state.date} min={minDate} max={maxDate} onChange={this.handleDate} />
                <button onClick={this.handleClick}>Dodaj zadanie</button>
                <hr />
            </div>
        )
    }
}

export default AddTask;