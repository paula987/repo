import React from 'react';


const Task = (props) => {
    const style = {
        color: 'red',
    }
    const { text, date, id, active, important, finishDate } = props.task
    if(active){
    return (
        <div>
            <p>
                <strong style={important ? style : null}>{text} - do: <span>{date}  </span> </strong>
                <button onClick={() => props.change(id)}>Dodaj do zrobionych</button>
                <button class="btn-delete" onClick={() => props.delete(id)}>Usuń</button>
            </p>
        </div>
    )
    }else {
        const finish = new Date(finishDate).toLocaleString()
        return(
        <div>
            <p>
        <strong>{text} </strong><em>(zrobić do: <span>{date}  </span>)</em><br></br>zadanie zrobione: {finish}
                <button class="btn-delete" onClick={() => props.delete(id)}>Usuń</button>
            </p>
        </div>
        )
    }
}

export default Task;