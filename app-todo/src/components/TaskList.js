import React from 'react';
import Task from './Task'

const TaskList = (props) => {
    const active = props.tasks.filter(task => task.active)
    const done = props.tasks.filter(task => !task.active)
    const activeTasks = active.map(task => <Task key={[task.id]} task={task} delete={props.delete} change={props.change} />)
    const doneTasks = done.map(task => <Task key={[task.id]} task={task} delete={props.delete} change={props.change} />)
    return (
        <>
            <div>
                <h2>Lista zadań</h2>
                {activeTasks.length > 0 ? activeTasks : <p>Brak zadań do zrobienia! :)</p>}
            </div>

            <hr />

            <div>
                <h4>Zadania zrobione <em>({done.length})</em></h4>
                {doneTasks}
            </div>
        </>
    )
}

export default TaskList;