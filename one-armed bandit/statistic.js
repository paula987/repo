class Statistic {
    constructor() {
        this.gameResult = []
    }
    addGameToStatistic(win, bid) {
        let gameResult = {
            win: win,
            bid: bid
        }
        this.gameResult.push(gameResult)
    }
    showGameStatistic() {
        let games = this.gameResult.length
        let wins = this.gameResult.filter(result => result.win).length
        let looses = this.gameResult.filter(result => !result.win).length
        return [games, wins, looses]
    }

}

const stats = new Statistic()