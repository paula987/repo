const reveal = () => {
    const reveals = document.querySelectorAll('.reveal')
    Array.from(reveals).map((el) => {
        if (el.getBoundingClientRect().top < window.innerHeight - 150) {
            el.classList.add('active')
        } else {
            el.classList.remove('active')
        }
    })
}
window.addEventListener('scroll', reveal)